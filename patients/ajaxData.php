<?php
//Include database configuration file
require __DIR__ . '../../lib/connect.php';

if(isset($_POST["department_id"]) && !empty($_POST["department_id"])){
    //Get all state data
    $query = $db->query("SELECT * FROM doctors WHERE department_id = ".$_POST['department_id']." AND status = 1 ORDER BY doctor_name ASC");
    
    //Count total number of rows
    $rowCount = $query->num_rows;
    
    //Display states list
    if($rowCount > 0){
        echo '<option value="">Select doctor</option>';
        while($row = $query->fetch_assoc()){ 
            echo '<option value="'.$row['doctor_id'].'">'.$row['doctor_name'].'</option>';
        }
    }else{
        echo '<option value="">Doctor not available</option>';
    }
}

if(isset($_POST["doctor_id"]) && !empty($_POST["doctor_id"])){
    //Get all city data
    $query = $db->query("SELECT * FROM dates WHERE doctor_id = ".$_POST['doctor_id']." AND status = 1 ORDER BY date_name ASC");
    
    //Count total number of rows
    $rowCount = $query->num_rows;
    
    //Display cities list
    if($rowCount > 0){
        echo '<option value="">Select Date</option>';
        while($row = $query->fetch_assoc()){ 
            echo '<option value="'.$row['date_id'].'">'.$row['date_name'].'</option>';
        }
    }else{
        echo '<option value="">Date not available</option>';
    }
}
?>