<?php
// Start Session
session_start();
// check user login
if(empty($_SESSION['id']))
{
    header("Location: ../login.php");
}
// Database connection
require __DIR__ . '../../lib/connect.php';
$db = DB();
// Application library ( with DemoLib class )
require __DIR__ . '../../lib/library.php';
$app = new CmsAdminLib();
$user = $app->UserDetails($_SESSION['id']); // get user details

?>
<?php 
$db = new PDO('mysql:host=localhost;dbname=cms;charset=utf8mb4', 'root', '');
$query = "SELECT * FROM `doctors`";
include '../header.php';  ?>
    
    <body>

        <div id="layout">

            <?php// include 'header-nav.php';  ?>
            <header>
    <div class="container">
        <div class="row">
         
        <div class="logo inner-logo">
            <a href="../index.php"><img src="images/logo-white.png" alt="logo" class="img-responsive"></a>
        </div>
         
         
        <div class="tools-top">
         
            <div class="avatar-profile">
                <div class="user-edit">
                    <h4><strong><?php echo $user->first_name ;?> <?php echo $user->last_name ;?></strong></h4>
                    <a href="my-account.php">edit profile</a>
                </div>
                <div class="avatar-image">

                    <img width="38px" height="38px" src="../admin/images/<?php echo $user->image; ?>"/>
                    <!-- <a href="appointments-reserved.php" title="2 Notifications Pending">
                    <span class="notifications" style="display: inline;">2</span>
                    </a> -->
                </div>
            </div>
             
            <ul class="tools-help">
                <li><a href="p_help.php" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="Help"><i class="fa fa-question-circle"></i></a></li>
                <li><a href="../logout.php" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="Logout"><i class="fa fa-sign-out"></i></a></li>
            </ul>
        </div>
         
        </div>
    </div>
</header>
 
<nav>
    <div class="container">
        <h4 class="navbar-brand">menu</h4>
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <i class="fa fa-bars" aria-hidden="true"></i>
            </button>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav main-menu">
                <li> <a href="my-account.php">my account</a> </li>
                <li class="active"> <a href="booked-calendar.php">book an appointment</a> </li>
                <li> <a href="appointments-reserved.php">appointments reserved</a> </li>
                <li> <a href="patients-examinations.php">Result Examinations</a> </li>
            </ul>
            
            </div>
        </div>
    </div>
</nav>

            <div class="main-container">
                <div class="container">
                    <div class="row">

                        <h3>Select Department and a Doctor then, You could add your appointment's date</h3>
                        <form action="store-appointment.php" method="post" class="make-app">
                            <div class="search-appointment" align="center">
                                <!--Departments-->
                                <div class="icon-data">
                                    <i class="fa fa-hospital-o"></i>*
                                </div>
                                
                                <select name="department">
                                    <option value="" selected="">Departments</option>
                                    <?php  
                                         foreach($db->query($query) as $doctors) { ?>
                                    <option value="<?= $doctors['department']; ?>"><?php echo $doctors['department'];?></option>
                                    <?php }
                                    ?>

                                </select>

                                <div class="icon-data">
                                    <i class="fa fa-user-md"></i>*
                                </div>
                                <select name="doctors">
                                    <option value="" selected="">Doctors</option>
                                    <?php  
                                         foreach($db->query($query) as $doctors) { ?>
                                    <option value="<?= $doctors['name']; ?>"><?php echo $doctors['name'];?></option>
                                    <?php }
                                    ?>

                                </select>

                                <div class="icon-data">
                                    <i class="fa fa-calendar-plus-o"></i>*
                                </div>
                                <input type="date" name="date" placeholder="select date">

                                <div class="icon-data">
                                    <i class="fa fa-clock-o"></i>*
                                </div>
                                <select name="time">
                                    <option value="" selected="">Select Time</option>
									<?php  
                                         foreach($db->query($query) as $doctors) { ?>
                                    <option value="<?= $doctors['time']; ?>"><?php echo $doctors['time'];?></option>
                                    <?php }
                                    ?>

                                </select>
                               <?php /* <div class="icon-data">
                                    <i class="fa fa-check-square-o"></i>*
                                </div>
                                <select name="type_of_appointment">
                                    <option value="" selected="">Type of Appointment</option>
                                    <option value="General">General</option>
                                    <option value="Specialist">Specialist</option>
                                </select>
*/ ?>
                                <span class="btn btn-green btn-small btn-search-appointment">
                                    <i class="fa fa-search"></i> 
                                     <button class="book-button" type="submit" name="submit" value="submit">Book</button>
                                </span>

                                <div class="preview-doctor">
                                    <a href="../meet-doctors.php">More About Doctors</a>
                                </div>
                            </div>
                         </form>
                    </div>


                </div>
            </div>
         </div>   
        <?php include '../footer.php';  ?>
