<?php
// Start Session
session_start();
// check user login
if(empty($_SESSION['id']))
{
    header("Location: ../login.php");
}
// Database connection
require __DIR__ . '../../lib/connect.php';
$db = DB();
// Application library ( with DemoLib class )
require __DIR__ . '../../lib/library.php';
$app = new CmsAdminLib();
$user = $app->UserDetails($_SESSION['id']); // get user details

?>
<?php

$query = "SELECT * FROM `reports`";
// var_dump($query);
include '../header.php'; 
?>


    <body>

        <div id="layout">

            <?php// include 'header-nav.php';  ?>
            <header>
    <div class="container">
        <div class="row">
         
        <div class="logo inner-logo">
            <a href="../index.php"><img src="images/logo-white.png" alt="logo" class="img-responsive"></a>
        </div>
         
         
        <div class="tools-top">
         
            <div class="avatar-profile">
                <div class="user-edit">
                    <h4><strong><?php echo $user->first_name ;?> <?php echo $user->last_name ;?></strong></h4>
                    <a href="my-account.php">edit profile</a>
                </div>
                <div class="avatar-image">

                    <img width="38px" height="38px" src="../admin/images/<?php echo $user->image; ?>"/>
                    <!-- <a href="appointments-reserved.php" title="2 Notifications Pending">
                    <span class="notifications" style="display: inline;">2</span>
                    </a> -->
                </div>
            </div>
             
            <ul class="tools-help">
                <li><a href="p_help.php" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="Help"><i class="fa fa-question-circle"></i></a></li>
                <li><a href="../logout.php" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="Logout"><i class="fa fa-sign-out"></i></a></li>
            </ul>
        </div>
         
        </div>
    </div>
</header>
 
<nav>
    <div class="container">
        <h4 class="navbar-brand">menu</h4>
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <i class="fa fa-bars" aria-hidden="true"></i>
            </button>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav main-menu">
                <li> <a href="my-account.php">my account</a> </li>
                <li> <a href="booked-calendar.php">book an appointment</a> </li>
                <li> <a href="appointments-reserved.php">appointments reserved</a> </li>
                <li class="active"> <a href="patients-examinations.php">Result Examinations</a> </li>
            </ul>
            
            </div>
        </div>
    </div>
</nav>

            <section class="container">
                <div class="main-container">
                    <div class="row">
                        <div class="listed">

                            <div class="row">
                                <div class="box-listed">
                                    
                                    <?php 
                                            foreach($db->query($query) as $reports) { ?>
                                    <ul class="list-unstyled box-item-list">
                                    
                                        <li>
                                            <i class="fa fa-file-text-o type-icon"></i>
                                            <span><?php echo date("d M Y", strtotime($reports['created_at'])); ?></span>
                                            <span class="type-test"><?php echo $reports['report_title'];?></span>
                                            <span class="download-link"><a href="admin/images/<?php echo $reports["report_file"]; ?>" download="" class="hvr-icon-down">Download</a></span>
                                        </li>
                                    </ul><?php }?>
                                </div>

                            </div>

                            <div class="row">
                                <div class="load-more">
                                    <a class="btn btn-green btn-small" href="#"> Load more</a>
                                </div>
                            </div>
                        </div>

                        <!--Aside-->
                        <aside>
                            <div class="elements-aside solid-color">
                                <ul>
                                    <li class="color-1">
                                        <i class="fa fa-heartbeat" aria-hidden="true"></i>
                                        <h4>Emergency Case</h4>
                                        <p>If you need a doctor urgently outside of medicenter opening hours, call emergency appointment number for emergency service.</p>
                                    </li>
                                    <li class="color-2">
                                        <i class="fa fa-hourglass-half" aria-hidden="true"></i>
                                        <h4>Working Time</h4>
                                        <p>Monday to Friday <span> 09:00am to 05:00pm</span></p>
                                        <p>Weekends <span> 09:00am to 12:00pm</span></p>
                                    </li>
                                    <li class="color-3">
                                        <i class="fa fa-id-badge" aria-hidden="true"></i>
                                        <h4>Medical Services</h4>
                                        <p>If you need a doctor urgently outside of medicenter opening hours, call emergency appointment number for emergency service.</p>
                                    </li>
                                </ul>
                            </div>
                        </aside>
                        <!--Aside-->
                    </div>
                </div>
            </section>

        </div>

    <?php include '../footer.php';  ?>

