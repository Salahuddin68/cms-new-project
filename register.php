<?php
// Start Session
session_start();

// Database connection
require __DIR__ . '/lib/connect.php';
$db = DB();

// Application library ( with CmsAdminLib class )
require __DIR__ . '/lib/library.php';
$app = new CmsAdminLib();


$register_error_message = '';



// check Register request
if (!empty($_POST['btnRegister'])) {
    if ($_POST['name'] == "") {
        $register_error_message = 'Name field is required!';
    } else if ($_POST['email'] == "") {
        $register_error_message = 'Email field is required!';
    } else if ($_POST['username'] == "") {
        $register_error_message = 'Username field is required!';
    } else if ($_POST['password'] == "") {
        $register_error_message = 'Password field is required!';
    } else if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
        $register_error_message = 'Invalid email address!';
    } else if ($app->isEmail($_POST['email'])) {
        $register_error_message = 'Email is already in use!';
    } else if ($app->isUsername($_POST['username'])) {
        $register_error_message = 'Username is already in use!';
    } else {
        $user_id = $app->Register($_POST['name'], $_POST['email'], $_POST['username'], $_POST['password']);
        // set session and redirect user to the profile page
        $_SESSION['user_id'] = $user_id;
        header("Location: profile.php");
    }
}
?>



<?php include 'header.php';  ?>

    <body class="panel-access">

        <div id="layout">
             <!--Login-->
                <div class="login">
                    <div class="container">
                        <div class="register-form">

                            <!--Data form-->
                            <div class="data-form">
                                <span class="back-to-login">
                                    <a class="btn btn-green btn-xsmall" href="login.php"><i class="fa fa-angle-double-left"></i> Back to Login</a>
                                </span>
                                <span class="back-to-homepage">
                                    <a class="btn btn-green btn-xsmall" href="index.php"><i class="fa fa-home"></i> Back to Homepage</a>
                                </span>
                                <!--Logo-->
                                <a href="register.php" class="logo reg-logo"><img src="images/login-logo.png" alt="logo"></a>
                                <!--Logo-->

                                <!--Form-->
                                <div class="form-login">
                                    <?php
                                        if ($register_error_message != "") {
                                        echo '<div class="alert alert-danger"><strong>Error: </strong> ' . $register_error_message . '</div>';
                                        }
                                    ?>
                                    <form name="form" action="admin/store-patient.php" method="post" enctype="multipart/form-data" class="form-horizontal">
                                        <div class="row">
                                            <h3>Personal Information</h3>
                                            
                                            <div class="form-group row">
                                              <div class="col-xs-6">
                                                <div class="icon-data">
                                                    <i class="fa fa-user"></i>
                                                </div>
                                                <input type="text" name="user_fname" placeholder="First Name" required="">
                                              </div>
                                              <div class="col-xs-6">
                                                <div class="icon-data">
                                                    <i class="fa fa-user"></i>
                                                </div>
                                                <input type="text" name="user_lname" placeholder="Last name" required="">
                                              </div>
                                            </div>
                                            <div class="form-group row">
                                              <div class="col-xs-6">
                                                <div class="icon-data">
                                                    <i class="fa fa-envelope"></i>
                                                </div>
                                                <input type="email" name="user_email" placeholder="Your Email" required="">
                                              </div>
                                              <div class="col-xs-6">
                                                <div class="icon-data">
                                                    <i class="fa fa-user"></i>
                                                </div>
                                                <input type="text" name="user_name" placeholder="Username" required="">
                                              </div>
                                            </div>
                                            <div class="form-group row">
                                              <div class="col-xs-6">
                                                <div class="icon-data">
                                                    <i class="fa fa-key"></i>
                                                </div>
                                                <input type="password"  name="user_pass" placeholder="Password" required="">
                                              </div>
                                              <div class="col-xs-6">
                                                <div class="icon-data">
                                                    <i class="fa fa-phone"></i>
                                                </div>
                                                <input type="number" name="user_num" placeholder="Your Phone" required="">
                                              </div>
                                            </div>
                                            <div class="form-group row">
                                              <div class="col-xs-6">
                                                <label>
                                                    <input type="radio" name="gender" value="female"> <span>Female</span>
                                                </label>
                                                <label>
                                                    <input type="radio" name="gender" value="male" > Male
                                                </label>
                                              </div>
                                              <div class="col-xs-6">
                                                <div class="avatar-profile">
                                                    <label>Upload Your Picture</label>
                                                    <input type="file" name="user_pic" >
                                                </div>
                                              </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-xs-12">
                                                    <button type="submit"  name="submit" value="submit" class="btn btn-default" onClick="Confirm(this.form)">Create Account</button>
                                                    <span class="help">
                                                        <a href="help.php" class="help-link">Help?</a>
                                                    </span>
                                                </div>
                                            </div>
                                              
                                        </div>
                                        
                                    </form>
                                </div>
                                <!--Form-->
                            </div>
                            <!--Data form-->
                        </div>
                    </div>
                </div>
                <!--Login-->

    <?php include 'footer.php';  ?>
    