 <footer>
    <div class="container">
        <div class="row">
            <div class="emergency-number">
                <p class="phone">+880-1813-123456</p>
                <h5><i class="fa fa-phone-square"></i> emergency contact</h5>
            </div>
            <div class="emergency-number email">
                <p>support@cms.com</p>
                <h5>online consultation <i class="fa fa-medkit"></i></h5>
            </div>
        </div>
    </div>
</footer>
<div class="copyrights">
    <div class="meet-social">
        <span><a href="#"><i class="fa fa-facebook-square"></i></a></span>
        <span><a href="#"><i class="fa fa-twitter-square"></i></a></span>
        <span><a href="#"><i class="fa fa-google-plus-square"></i></a></span>
        <span><a href="#"><i class="fa fa-linkedin-square"></i></a></span>
    </div>
    <p>© 2017 - <a href="#" target="_blank">CLINIC MANAGEMENT SYSTEM</a>. All Rights Reserved.</p>
</div>
<script type="text/javascript" src="js/jquery.min.js"></script>
<!-- <script type="text/javascript" src="js/jquery-ui.min.js"></script> -->
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/custom.js"></script>
<script type="text/javascript">
    // $('.main-menu').on('click','li', function(){
    //     $(this).addClass('active').siblings().removeClass('active');
    // });
</script>

</body>
</html>