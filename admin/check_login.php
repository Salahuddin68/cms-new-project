<?php
/*
$db = new PDO('mysql:host=localhost;dbname=cms;charset=utf8mb4', 'root', '');
$name = $_POST['user_name'];
$password = ($_POST['password']);
$query = mysqli_query($db,"SELECT * FROM admin WHERE user_name = '$name' && password = '$password' ");
$result = $db->exec($query);

$database_connect = mysqli_connect($hostname, $username, $password, $dbname);

if( (empty ($name)) || (empty ($password))){
	header("location:index.php?error_msg=user name and password is requerd"); 
}

else
{

	

	$no_rows = mysqli_num_rows($query);
	
	if($result == 1){
		// session_register('name');
		$_SESSION['name']="user_name";
		$_SESSION['password']="password";
		//session_register('password');
		header("location:dashboard.php");
	}
	else{
		header("location:index.php?error_msg=your user name or password is invalid");	
	}
	
}
*/
?>

<?php
session_start();
$errmsg_arr = array();
$errflag = false;
// configuration
$dbhost 	= "localhost";
$dbname		= "cms";
$dbuser		= "root";
$dbpass		= "";

// database connection
$conn = new PDO("mysql:host=$dbhost;dbname=$dbname",$dbuser,$dbpass);

// new data

$user = $_POST['username'];
$password = $_POST['password'];

if($user == '') {
	$errmsg_arr[] = 'You must enter your Username';
	$errflag = true;
}
if($password == '') {
	$errmsg_arr[] = 'You must enter your Password';
	$errflag = true;
}

// query
$result = $conn->prepare("SELECT * FROM admin WHERE username= :admin AND password= :123456");
$result->bindParam(':admin', $user);
$result->bindParam(':123456', $password);
$result->execute();
$rows = $result->fetch(PDO::FETCH_NUM);
if($rows > 0) {
header("location: dashboard.php");
}
else{
	$errmsg_arr[] = 'Your username or password is invalid';
	$errflag = true;
}
if($errflag) {
	$_SESSION['error_msg'] = $errmsg_arr;
	session_write_close();
	header("location: index.php");
	exit();
}

?>