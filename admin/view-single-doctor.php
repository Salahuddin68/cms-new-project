<?php
$db = new PDO('mysql:host=localhost;dbname=cms;charset=utf8mb4', 'root', '');
$query = "SELECT * FROM `doctors` WHERE id =".$_GET['id'];
// var_dump($query);
foreach($db->query($query) as $row) {
  $doctors = $row;
}
include 'header.php'; 
?>



<div id="wrapper">
    <!-- Navigation -->
    <?php include 'navigation.php'; ?>

    <div id="page-wrapper">
        <div class="container-fluid" style="margin-top: 20px;">
        <a href="create-doctor.php" class="btn btn-success"><i class="fa fa-plus"></i> Add New Doctor</a>
        <a href="view-doctor-list.php" class="btn btn-info"><i class="fa fa-eye"></i> View All Doctor</a>      
          

            <div class="row" style="margin-top: 40px;">
              <div class="col-md-4">
                <img  src="images/<?php echo $doctors["picture"]; ?>" alt="" class="img-thumbnail profile-image">
                
              </div>
              <div class="col-md-8">
                
                <div class="pro-desc">
                  <h3><strong><?php echo $doctors['name'];?></strong></h3>

                  <table class="table table-striped">
                    <tbody>
                     <tr>
                      <td><strong>Specialization</strong></td>
                      <td><?php echo $doctors['specialist'];?></td>
                    </tr>
                    <tr>
                      <td><strong>Department</strong></td>
                      <td><?php echo $doctors['department'];?></a></td>
                    </tr>
                    <tr>
                      <td><strong>Date</strong></td>
                      <td><?php echo $doctors['available_date'];?></td>
                    </tr>
                    <tr>
                      <td><strong>Time</strong></td>
                      <td><?php echo $doctors['time'];?></td>
                    </tr>
                    <tr>
                      <td><strong>Office no.</strong></td>
                      <td><?php echo $doctors['office'];?></td>
                    </tr>
                    <tr>
                      <td><strong>Biodata</strong></td>
                      <td><?php echo $doctors['biodata'];?></td>
                    </tr>
                    <tr>
                      <td><strong>Created Date</strong></td>
                      <td><?php echo date("d/m/Y", strtotime($doctors['created_at']));?></td>
                    </tr>
                    <tr>
                      <td><strong>Modification Date</strong></td>
                      <td><?php echo date("d/m/Y", strtotime($doctors['modified_at']));?></td>
                    </tr>
                    </tbody>
                  </table>
                </div>
              </div>
          </div>

        </div>
    </div>
</div>

<?php include 'footer.php'; ?>
