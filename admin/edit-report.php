<?php
$db = new PDO('mysql:host=localhost;dbname=cms;charset=utf8mb4', 'root', '');
$query = "SELECT * FROM `reports` WHERE id =".$_GET['id'];
// var_dump($query);
include 'header.php'; 
foreach($db->query($query) as $row) {
  $reports = $row;
}
?>

<div id="wrapper">
    <!-- Navigation -->
    <?php include 'navigation.php'; ?>

    <div id="page-wrapper">
        <div class="container-fluid" style="margin-top: 20px;">      
           <a href="add-report.php" class="btn btn-success"><i class="fa fa-plus"></i> Add New Report</a>
           <a href="view-report-list.php" class="btn btn-info"><i class="fa fa-eye"></i> view all Report</a>
            <div class="row">
                <div class="col-md-12" id="doctor-info-update">
                    
                        <h2>reports Account Setting:</h2>
                       <form action="update-report.php" method="post" enctype="multipart/form-data">
                            <input type="hidden" class="form-control" id="id" name="id" value="<?= $reports['id']; ?>">
                            <div class="form-group">
                                <label class="control-label col-sm-3" >Patient ID:</label>
                                <div class="col-sm-9">
                                    <input class="form-control" name="patient_id" value="<?php echo $reports['patients_id']; ?>" type="text" required="">
                                </div>
                            </div>
                           <div class="form-group">
                                <label class="control-label col-sm-3" >Patient Name :</label>
                                <div class="col-sm-9">
                                    <input class="form-control" name="patient_name" value="<?php echo $reports['patients_name']; ?>" type="text" required="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3" >Report Title:</label>
                                <div class="col-sm-9">
                                    <input class="form-control" name="report_title" value="<?php echo $reports['report_title']; ?>"  type="text" required="">
                                </div>
                            </div>
                    
                           <!--  <div class="form-group">
                                <label class="control-label col-sm-3">Address:</label>
                                <div class="col-sm-9">
                                    <input class="form-control" name="user_address" value="<?php echo $reports['user_address']; ?>" placeholder="Enter Address" type="text">
                                </div>
                            </div> -->
                           
                           
                            <div class="form-group">
                                
                                <label class="col-sm-3 control-label">Upload Report File:</label>
                                <div class="col-sm-9">
                                    <img width="100px" style="margin-bottom: 15px;" src="images/<?php echo $reports["report_file"]; ?>"/>
                                    <input type="file" name="report_pic">
                                </div>
                                
                            </div>

                            <div class="form-group" style="margin-top:20px">
                                <div class="col-sm-9 col-sm-offset-3">
                                    <button type="submit" name="submit" value="submit" class="btn btn-primary">Submit</button>
                                    <button type="reset" class="btn btn-warning">Reset</button>
                                </div>
                            </div>
                       </form>
                  
                </div>
          </div>
        </div>
    </div>
</div>

<?php include 'footer.php'; ?>