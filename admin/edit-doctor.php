<?php
$db = new PDO('mysql:host=localhost;dbname=cms;charset=utf8mb4', 'root', '');
$query = "SELECT * FROM `doctors` WHERE id =".$_GET['id'];
// var_dump($query);
include 'header.php'; 
foreach($db->query($query) as $row) {
  $doctors = $row;
}
?>

<div id="wrapper">
    <!-- Navigation -->
    <?php include 'navigation.php'; ?>

    <div id="page-wrapper">
        <div class="container-fluid" style="margin-top: 20px;">      
           <a href="create-doctor.php" class="btn btn-success"><i class="fa fa-plus"></i> Add New doctor</a>
           <a href="view-doctor-list.php" class="btn btn-info"><i class="fa fa-eye"></i> view all doctor</a>
            <div class="row">
                <div class="col-md-12" id="doctor-info-update">
                    
                        <h2>doctors Account Setting:</h2>
                       <form action="update-doctor.php" method="POST" enctype="multipart/form-data">
                            <div class="form-group">
                                <label class="control-label col-sm-3" >ID:</label>
                                <div class="col-sm-9">
                                    <input class="form-control" name="id" value="<?php echo $doctors['id']; ?>" type="text" required="">
                                </div>
                            </div>
                          <div class="form-group">
                                <label class="control-label col-sm-3" >Name:</label>
                                <div class="col-sm-9">
                                    <input class="form-control" name="user_name" value="<?php echo $doctors['name']; ?>" placeholder="Name" type="text" required="">
                                </div>
                            </div>
                             <div class="form-group">
                                <label class="control-label col-sm-3" >Specialization:</label>
                                <div class="col-sm-9">
                                    <input class="form-control" name="user_special" value="<?php echo $doctors['specialist']; ?>"" placeholder="Specialization" type="text" required="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3" >Department:</label>
                                <div class="col-sm-9">
                                    <input class="form-control" name="user_department" value="<?php echo $doctors['department']; ?>"" placeholder="Department" type="text" required="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">Available Date:</label>
                                <div class="col-sm-9">
                                    <input class="form-control"  name="user_available_date" value="<?php echo $doctors['available_date']; ?>"" placeholder="Select Date" type="Date" required="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">Choose Time:</label>
                                <div class="col-sm-9">
                                    <label class="checkbox-inline"><input type="checkbox" name="ctime" value="10am-11am"<?php if (preg_match("/10am-11am/", $doctors['time'] )) { echo "checked"; } else{ echo " ";}?> >10am-11am</label>
                                    
                                    <label class="checkbox-inline"><input type="checkbox" name="ctime" value="11am-12pm" <?php if (preg_match("/11am-12pm/", $doctors['time'] )) { echo "checked"; } else{ echo " ";}?>>11am-12pm</label>
                                    <label class="checkbox-inline"><input type="checkbox" name="ctime" value="3pm-5pm"<?php if (preg_match("/3pm-5pm/", $doctors['time'] )) { echo "checked"; } else{ echo " ";}?>>3pm-5pm</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">Doctor Office:</label>
                                <div class="col-sm-9">
                                    <select class="form-control" name="doc_office">
                                        <option  <?php if($doctors['office'] == 202) echo 'selected="selected"'; ?>>202</option>
                                        <option <?php if($doctors['office'] == 203) echo 'selected="selected"'; ?>>203</option>
                                        <option <?php if($doctors['office'] == 204) echo 'selected="selected"'; ?>>204</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">Doctor Biodata:</label>
                                <div class="col-sm-9">
                                    <input class="form-control" name="biodata" value="<?php echo $doctors['biodata']; ?>" placeholder="Enter biodata" type="text">
                                </div>
                            </div>
                           <div class="form-group">
                                
                                <label class="col-sm-3 control-label">Upload File:</label>
                                <div class="col-sm-9">
                                    <img width="100px" style="margin-bottom: 15px;" src="images/<?php echo $doctors["picture"]; ?>"/>
                                    <input type="file" name="user_pic" value="<?php echo $doctors['picture']; ?>" >
                                </div>
                                
                            </div>

                            <div class="form-group" style="margin-top:20px">
                                <div class="col-sm-9 col-sm-offset-3">
                                    <button type="submit" name="submit" value="submit" class="btn btn-success">Submit</button>
                                    <button type="reset" class="btn btn-warning">Reset</button>
                                </div>
                            </div>
                        </form>
                     </fieldset>
                  </div>
              </div>
            </div>
        </div>
    </div>

<!-- <script src="ckeditor/ckeditor.js"></script>
<script src="ckeditor/editor.js"></script> -->

<?php include 'footer.php'; ?>