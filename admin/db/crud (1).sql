-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 02, 2017 at 09:01 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `crud`
--

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE `course` (
  `id` int(20) NOT NULL,
  `subject_code` int(20) NOT NULL,
  `subject_title` varchar(255) NOT NULL,
  `department` varchar(255) NOT NULL,
  `shift` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `course`
--

INSERT INTO `course` (`id`, `subject_code`, `subject_title`, `department`, `shift`) VALUES
(1, 1234, 'PHP48', 'WEB4', 'Day'),
(4, 123, 'PHP', 'WEB2', 'Evening'),
(5, 123, 'JAVA2', 'android', 'Day'),
(6, 123, 'java4', 'android4', 'Evening');

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id` int(20) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_seip` int(20) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_number` int(11) NOT NULL,
  `user_address` varchar(255) NOT NULL,
  `user_gender` varchar(255) NOT NULL,
  `user_pic` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `modified_at` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `user_name`, `user_seip`, `user_email`, `user_number`, `user_address`, `user_gender`, `user_pic`, `created_at`, `modified_at`) VALUES
(17, 'Jalal Ahamad', 168280, 'jalal@gmail.com', 1813123456, 'dhaka', 'Male', 'SEID-168280.jpg', '2017-05-02 21:16:35.000000', '2017-05-03 00:49:03.000000'),
(23, 'jalal ahamad', 545, 'sd8@sd.com', 181236598, 'mohammadpur', 'Male', 'love-485a.jpg', '2017-05-02 22:08:06.000000', '2017-05-03 00:52:55.000000'),
(29, 'Engineer Jalal', 4875, 'jalal2@gmail.com', 9875487, 'dhanmondi', 'Male', 'jalalvai.jpg', '2017-05-03 00:57:22.000000', '2017-05-03 00:58:59.000000');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `course`
--
ALTER TABLE `course`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `course`
--
ALTER TABLE `course`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
