   <?php include 'header.php';  ?>

    <body class="body-home">
        <header class="header">
            <div class="menu">
                <div class="container-fluid">
                    <div class="row">
                        <div class="logo home">
                            <a href="index.php"><img src="images/logo-white.png" alt="logo main" class="img-responsive"></a>
                        </div>

                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                              <i class="fa fa-bars" aria-hidden="true"></i>
                            </button>
                        </div>

                        <div class="menu-nav">
                            <div class="navbar-collapse collapse">
                                <ul>
                                    <li><a href="meet-doctors.php">Doctors</a></li>
                                    <li><a href="register.php">Register</a></li>
                                    <li><a href="login.php">Login</a></li>
                                    <li><a href="blog.php">Blog</a></li>
                                    <li><a href="#how-to">How to Use?</a></li>
                                    <li><a href="help.php">Help</a></li>
                                    <li class="live-dashboard btn-primary">
                                        <a href="patients/appointments-reserved.php">View Dashboard</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="meet-social">
                            <span><a href="#"><i class="fa fa-facebook-square"></i></a></span>
                            <span><a href="#"><i class="fa fa-twitter-square"></i></a></span>
                            <span><a href="#"><i class="fa fa-google-plus-square"></i></a></span>
                            <span><a href="#"><i class="fa fa-linkedin-square"></i></a></span>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="hero-msj">
                            <h1>Now, you can book your <br>appointment Online!</h1>
                            <p>Clinic Appointment Management System</p>
                            <a href="login.php" class="btn btn-green btn-small" role="button">Book an Appointment</a>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <section class="container">
            <div class="row">
                <div class="elements-aside services solid-color">
                    <ul>
                        <li class="color-1">
                            <i class="fa fa-heartbeat" aria-hidden="true"></i>
                            <h4>Book an Appointment</h4>
                            <p>Now, you can book your appointment online! Just register or Login</p>
                            <a href="login.php" class="btn btn-red btn-xsmall" role="button">Book an Appointment</a>
                        </li>
                        <li class="color-2">
                            <i class="fa fa-hourglass-half" aria-hidden="true"></i>
                            <h4>Working Time</h4>
                            <p>Monday to Friday <span> 09:00am to 05:00pm</span></p>
                            <p>Weekends <span> 09:00am to 12:00pm</span></p>
                        </li>
                        <li class="color-1">
                            
                            <h4>Emergency Case</h4>
                            <p>If you need a doctor urgently outside of medicenter opening hours, call emergency appointment number for emergency service.</p>
                            <div class="emergency-number">
                                <p class="phone">+880-1813-123456</p>
                                <h5><i class="fa fa-phone"></i> emergency contact</h5>
                            </div>
                        </li>
                    </ul>
                </div>

                <div class="services-app">
                    <img src="images/icon-edit.png" alt="icon edit" class="img-responsive">
                    <h4>Modify, Cancell, Add Calendar or Print</h4>
                    <p>Now, You can create your appointment online also modify, Cancell, Add Calendar, Print it. Where you want!</p>
                </div>
                <div class="services-app">
                    <img src="images/icon-doctor.png" alt="icon edit" class="img-responsive">
                    <h4>Find Doctors</h4>
                    <p>MAS has a system where you can know more about doctors, biography, specializations and a pic where you feel confortable to choose who you want.</p>
                </div>
                <div class="services-app">
                    <img src="images/icon-chat.png" alt="icon edit" class="img-responsive">
                    <h4>Doubts?</h4>
                    <p>CMS has a system where you can chat with a person that is going to help you about how to use the platform and get an appointment.</p>
                </div>
                <div class="services-app">
                    <img src="images/icon-results.png" alt="icon edit" class="img-responsive">
                    <h4>Check status of Examination Medical </h4>
                    <p>You could see the result of examinations and authorizations pending. Everything Online!</p>
                </div>
            </div>
        </section>

        <section class="account-zone">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <div class="about-us-section">
                            <h3>About Us</h3>
                            <div class="col-md-6">
                                <img src="images/about-us.png" alt="doctor team" class="img-responsive">
                            </div>
                            <div class="col-md-6">
                                <p>Clinic Appointment Management System aims to help the Clinic patients to take appointment online through internet and track their records through it. PolyClinic is web based application which covers all aspects of management and operations of clinics. This website covers Doctors Details, Patients Records, Online appointments, Patient reports, billings, Clinical tests, etc. By using Clinic Appointment Management System, a clinic can better and more optimally manage the availability of their resources; it does this by integrating with the clinic’s existing appointment system.</p>
                            </div>
                        </div>
                    </div>

                    

                    <!--Accordion-->
                    <div class="form-intials accordion">
                        <h3>Departments</h3>
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                          <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                              <h5 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                  Cardiac Clinic
                                </a>
                              </h5>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                              <div class="panel-body">
                                The Cardiac Clinic have special programmes for clients designed to prevent cardiac problems and assist following a cardiac event like a heart attack. We put our heart into protecting yours.
                              </div>
                            </div>
                          </div>
                          <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                              <h5 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                  General Surgery
                                </a>
                              </h5>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo" aria-expanded="false" style="height: 0px;">
                              <div class="panel-body">
                                General surgery is a discipline that requires knowledge of and responsibility for the preoperative, operative, and postoperative management of patients. We put our heart into protecting yours.
                              </div>
                            </div>
                          </div>
                          <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingThree">
                              <h5 class="panel-title">
                                <a class="" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
                                  Pediatrics
                                </a>
                              </h5>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingThree" aria-expanded="true">
                              <div class="panel-body">
                                The pediatrics hub contains articles on immunization, growth and development, childhood diseases, and neonatology. The Journal of Pediatrics proudly announces that research articles identified by the American Board.
                              </div>
                            </div>
                          </div>
                        </div>
                    </div>
                    <!--Accordion-->

                </div>
            </div>
        </section>

        <section class="blog preview" id="blog">
            <div class="container">
                <h2>From Our Blog</h2>

                <div class="row">
                    <!--Item-->
                    <div class="item-blog">
                        <div class="item-blog-content">
                            <img src="images/welcome-login.jpg" alt="blog" class="img-responsive">
                            <h4><a href="blog.php">Who are our Doctors?</a></h4>
                            <span class="entry-category"><a href="#">Reviews &amp; Opinion</a></span>
                            <p>A physician is a practitioner who practices medicine and seeks to maintain and restore human health through the study, diagnosis, and treatment of the patient's illness or injury.</p>
                            <p class="social-media">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-google-plus"></i></a>
                            </p>
                            <p class="date">27 March 2017</p>
                        </div>
                    </div>
                    <!--Item-->

                    <!--Item-->
                    <div class="item-blog">
                        <div class="item-blog-content">
                            <img src="images/book.jpg" alt="blog" class="img-responsive">
                            <h4><a href="blog.php">How to use our Platform?</a></h4>
                            <span class="entry-category"><a href="#">Reviews &amp; Opinion</a></span>
                            <p>A physician is a practitioner who practices medicine and seeks to maintain and restore human health through the study, diagnosis, and treatment of the patient's illness or injury.</p>
                            <p class="social-media">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-google-plus"></i></a>
                            </p>
                            <p class="date">27 March 2017</p>
                        </div>
                    </div>
                    <!--Item-->

                    <!--Item-->
                    <div class="item-blog">
                        <div class="item-blog-content">
                            <img src="images/medication.jpg" alt="blog" class="img-responsive">
                            <h4><a href="blog.php">Right uses of medicine.</a></h4>
                            <span class="entry-category"><a href="#">Reviews &amp; Opinion</a></span>
                            <p>A physician is a practitioner who practices medicine and seeks to maintain and restore human health through the study, diagnosis, and treatment of the patient's illness or injury.</p>
                            <p class="social-media">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-google-plus"></i></a>
                            </p>
                            <p class="date">27 March 2017</p>
                        </div>
                    </div>
                    <!--Item-->
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <a href="blog.php" class="btn btn-red btn-small">view all entries</a>
                    </div>
                </div>
            </div>
        </section>

        <section class="how-to" id="how-to">
            <div class="container">
                <div class="intro-how-to">
                    <h2>How to use our Platform?</h2>
                    <p>Are not sure how to use our Platform? Here, we're going to do more details about how to do it. Just click it!</p>
                    <div class="row">
                        <div class="facts">
                            <span>100%</span>
                            <p>Usable</p>
                        </div>
                        <div class="facts">
                            <span>10k</span>
                            <p>User Registred</p>
                        </div>
                        <div class="facts">
                            <span>100%</span>
                            <p>Mobile</p>
                        </div>
                    </div>
                    <a href="login.php" class="btn btn-green btn-small" role="button">Get my Appointment</a>
                </div>
                <div class="video-how-to">
                    <div class="embed-responsive embed-responsive-16by9">
                      <iframe  class="embed-responsive-item" src="https://www.youtube.com/embed/vpfjo0DLbTw" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </section>

    <?php include 'footer.php';  ?>