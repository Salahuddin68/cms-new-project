<?php include 'header.php';  ?>

    <body>
        <div id="layout">
             <header>
                <div class="menu">
                    <div class="container">
                        <div class="row">
                            <div class="logo inner-logo">
                               <a href="index.php"><img src="images/logo-white.png" alt="logo main" class="img-responsive"></a>
                            </div>
                            <div class="meet-social social-info">
                                <span><a href="https://www.facebook.com/cmsof007/"><i class="fa fa-facebook-square"></i></a></span>
                                <span><a href="https://twitter.com/CMS_007"><i class="fa fa-twitter-square"></i></a></span>
                                <span><a href="#"><i class="fa fa-google-plus-square"></i></a></span>
                                <span><a href="https://www.linkedin.com/in/clinic-management-management-347297144/"><i class="fa fa-linkedin-square"></i></a></span>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <!--Menu-->
            <nav>
                <div class="container">
                    <h4 class="navbar-brand">menu</h4>
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                          <i class="fa fa-bars" aria-hidden="true"></i>
                        </button>
                    </div>

                    <div class="navbar-collapse collapse">

                        <ul class="nav navbar-nav">
                            <li> <a href="index.php">Home</a> </li>
                            <!-- <li> <a href="appointments-reserved.php">appointments reserved</a> </li> -->
                            <!-- <li> <a href="booked-calendar.php">book an appointment</a> </li> -->
                            <li> <a href="login.php">book an appointment</a> </li>
                            <!-- <li> <a href="examinations.php">Result Examinations</a> </li>
                            <li> <a href="my-account.php">my account</a> </li> -->
                            <li class="active"> <a href="help.php">Help</a> </li>
                        </ul>
                       
                    </div>
                </div>
            </nav>
            <!--Menu-->
            <div class="container">
                <div class="main-container">
                    <h2>Contact Us</h2>
                    <p>To us is a pleasure to help you. For that reason, we offer different ways to reply all your request you have. Also, if you need more help you could write us to our email <a href="/help.php#">support@cms.com</a> over there we're going to reply fast as we can.</p>

                    <div class="row">
                        <!--Services-->
                        <div class="services-help">
                            <div class="service-item">
                                <i class="fa fa-volume-control-phone" aria-hidden="true"></i>
                                <h4 data-toggle="modal" data-target="#callcenter">Call Center</h4>
                                <p>+8801813323479,
                                +8801813098910

                                </p>
                            </div>
                        </div>
                        <!--Services-->

                        <!--Services-->
                        <div class="services-help">
                            <div class="service-item">
                                <i class="fa fa-pencil" aria-hidden="true"></i>
                                <h4 data-toggle="modal" data-target="#suggession">Suggestions and Request</h4>
                                <p>Email = jalal@gmail.com</p>
                            </div>
                        </div>
                        <!--Services-->

                        <!--Services-->
                        <div class="services-help">
                            <div class="service-item">
                                <i class="fa fa-comments" aria-hidden="true"></i>
                                <h4 data-toggle="modal" data-target="#chatus">Chat with us</h4>
<span><a href="https://www.facebook.com/cmsof007/"><i class="fa fa-facebook-square"></i></a></span>
                            </div>
                        </div>
                        <!--Services-->

                        <!--Services-->
                        <div class="services-help">
                            <div class="service-item">
                                <i class="fa fa-question-circle" aria-hidden="true"></i>
                                <h4 data-toggle="modal" data-target="#faqs">Faqs</h4>
                                <p>You can question in our social site.</p>
                            </div>
                        </div>
                        <!--Services-->
                    </div>
                </div>
            </div>

            <div id="callcenter" class="modal fade" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close close-button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h4 class="modal-title" id="myModalLabel">Call center Information</h4>
                        </div>
                        <div class="modal-body">
                            <h4>+880-1813-123456</h4> 
                            <p>EMERGENCY CONTACT</p>
                            <p> You can call with us by using this Number <a href="#">+8801813323479,
                                +8801813098910</a> over there we're going to reply fast as we can.</p>
                        </div>
                    </div>
                </div>
            </div>

             <div id="suggession" class="modal fade" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close close-button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h4 class="modal-title" id="myModalLabel">Suggession information</h4>
                        </div>
                        <div class="modal-body">
                            <p> You can suggesst us by using this mail "jalal@gmail.com"<a href="https://accounts.google.com/ServiceLogin/identifier?service=mail&passive=true&rm=false&continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&ss=1&scc=1&ltmpl=default&ltmplcache=2&emr=1&osid=1&flowName=GlifWebSignIn&flowEntry=AddSession">Jalal@gmail.com</a> over there we're going to reply fast as we can.</p>
                        </div>
                    </div>
                </div>
            </div>

             <div id="chatus" class="modal fade" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close close-button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h4 class="modal-title" id="myModalLabel">Chatting information</h4>
                        </div>
                        <div class="modal-body">
                            <p>You can chat with us by using this fb page <a href="https://www.facebook.com/cmsof007/">https://www.facebook.com</a> over there we're going to reply fast as we can.</p>
                        </div>
                    </div>
                </div>
            </div>

             <div id="faqs" class="modal fade" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close close-button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h4 class="modal-title" id="myModalLabel">FAQS information</h4>
                        </div>
                        <div class="modal-body">
                            <p>To us is a pleasure to help you. For that reason, we offer different ways to reply all your request you have. Also, if you need more help you could write us to our email <a href="#">support@cms.com</a> over there we're going to reply fast as we can.</p>
                        </div>
                    </div>
                </div>
            </div>

            

    </div>

<?php include 'footer.php';  ?>

  
